<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
if($show_button) {
  $status_class = 'medium-7';
} else {
  $status_class = 'medium-9';
}
?>

<div class="foundation-block-progress">
<div class="foundation-block--progress">
  <div class="promo-bar">

    <div class="row" data-equalizer>
      <div class="column medium-3" style="display: table; padding: 1rem;" data-equalizer-watch>
        <h2 class="promo-bar-title" style="display: table-cell; vertical-align: middle;"><?php echo $title; ?></h2>
      </div>
      <div class="<?php echo $status_class; ?> column promo-bar-status" style="display: table" data-equalizer-watch>
        <div class="progress-meter-wrapper"  style="display: table-cell; vertical-align: middle;">
          <p><?php echo $description; ?></p>
          <div class="progress large" role="progressbar" tabindex="0" aria-valuenow="<?php echo $current_value; ?>" aria-valuemin="<?php echo $min_value ?>" aria-valuetext="<?php echo $current_value; ?>" aria-valuemax="<?php echo $max_value; ?>">
            <span class="progress-meter" style="width: <?php echo $slider_value; ?>%">
              <p class="progress-meter-text"><?php echo $label_value; ?></p>
            </span>
          </div>
        </div>
      </div>
      <?php if($show_button) { ?>
        <div class="column medium-2 promo-bar-donate" style="display: table" data-equalizer-watch>
            <a href="<?php echo $link_url; ?>" style="width: 100%; display: table-cell; vertical-align: middle;"><?php echo $link_text; ?></a>
        </div>
      <?php } ?>
    </div>

  </div>
</div>
</div>

