// Using the closure to map jQuery to $.
(function ($) {
  // Store our function as a property of Drupal.behaviors.
  Drupal.behaviors.foundation_block_progress = {
    attach: function (context, settings) {
      var display_min = 'columns';
      var min_classes = $('#edit-display-options-min-label-classes').val().split(',');
      var max_classes = $('#edit-display-options-max-label-classes').val().split(',');
      $('#edit-display-options-show-min-options :checkbox').click(function() {
        var check_id = this.id.replace('edit-display-options-show-', '');
        if ($(this).is(':checked')) {
          switch(check_id) {
            case 'min-options-small':
              change_class(min_classes, 'hide-for-small', 'show-for-small small-12');
              console.log(min_classes);
              break;
            case 'min-options-medium':
              change_class(min_classes, 'hide-for-medium', 'show-for-medium medium-12');
              console.log(min_classes);
              break;
            case 'min-options-large':
              change_class(min_classes, 'hide-for-large', 'show-for-large large-12');
              console.log(min_classes);
              break;
            default:
              break;
          }
          console.log('#' + this.id + ' is checked.');
        } else {
          switch(check_id) {
            case 'min-options-small':
              remove_class(min_classes, 'small-12');
              console.log(min_classes);
              break;
            case 'min-options-medium':
              remove_class(min_classes, 'medium-12');
              console.log(min_classes);
              break;
            case 'min-options-large':
              remove_class(min_classes, 'large-12');
              console.log(min_classes);
              break;
            default:
              break;
          }
        }
        $('#edit-display-options-min-label-classes').val(min_classes);
      });
    }
  };
function remove_class($arr, $class) {
  for(var i = $arr.length - 1; i >= 0; i--) {
    if($arr[i] === $class) {
      $arr.splice(i, 1);
    }
  }
}
function add_class($arr, $class) {
  $arr.push($class);
}
function change_class($arr, $old, $new) {
  console.log($old);
  console.log($new);
  $.each($arr, function (key, val) {
    // search for value and replace it
    $arr[key] = val.replace($old, $new);
  })

}
})(jQuery);

