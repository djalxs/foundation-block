<?php

/**
 * @file
 * Describe entity and controller custom classes.
 */

/**
 * Foundation Block class.
 */
class FoundationBlock extends Entity {
  /**
   * Define the label of the entity.
   */
  protected function defaultLabel() {
    if (!isset($this->title)) {
      return '';
    }
    return $this->title;
  }

  /**
   * Specify the default uri, which is picked up by uri() by default.
   */
  protected function defaultUri() {
    return array('path' => 'admin/foundation/blocks/' . $this->identifier());
  }
}

/**
 * Foundation Block controller class.
 */
class FoundationBlockController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }
  /**
   * Build a structured array representing the entity's content.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('foundation_block', $entity);

    // Make "title" and "description" properties themed like default fields.
    $content['title'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' => t('Title'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_title',
      '#field_type' => 'text',
      '#entity_type' => 'foundation_block',
      '#bundle' => $wrapper->type->value(),
      '#items' => array(array('value' => $wrapper->title->value())),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($wrapper->title->value())),
    );
    $content['description'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' => t('Description'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_description',
      '#field_type' => 'text',
      '#entity_type' => 'foundation_block',
      '#bundle' => $wrapper->type->value(),
      '#items' => array(array('value' => $wrapper->description->value())),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($wrapper->description->value())),
    );
    $content['data'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' => t('Data'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_fake_data',
      '#field_type' => 'text',
      '#entity_type' => 'foundation_block',
      '#bundle' => $wrapper->type->value(),
      '#items' => array(array('value' => $wrapper->data->value())),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($wrapper->data->value())),
    );

    $content = parent::buildContent($entity, $view_mode, $langcode, $content);

    return $content;
  }
}
