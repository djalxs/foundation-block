<?php

/**
 * @file
 * Callbacks for administrating entities.
 */
/**
 * Implements hook_permission().
 */
function foundation_block_permission() {
  $permissions = array(
    'view foundation blocks' => array(
      'title' => t('View Typical Entity Example content'),
    ),
    'administer foundation blocks' => array(
      'title' => t('Administer Typical Entity Example content'),
      'restrict access' => TRUE,
    ),
  );
  return $permissions;
}

/**
 * Choose bundle of entity to add.
 *
 * @return array
 *   Array describing a list of bundles to render.
 */
function foundation_block_choose_bundle() {
  drupal_set_title(t('Choose type of entity to add.'));

  // Show list of all existing entity bundles.
  $entity_info = entity_get_info('foundation_block');
  $items = array();
  foreach ($entity_info['bundles'] as $bundle_name => $bundle_data) {
    $items[] = l($bundle_data['label'], 'admin/foundation/blocks/add-' . $bundle_name);
  }
  return array(
    'list' => array(
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => t('Choose type of entity to add.'),
    ),
  );
}

/**
 * Form constructor for the entity add form.
 *
 * @param string $type
 *   Bundle of the entity to add.
 *
 * @return array
 *   Entity edit form.
 */
function foundation_block_add($type) {
  $type = str_replace('add-', '', $type);
  $type = str_replace('-', '_', $type);
  drupal_set_title(t('Create new %type', array('%type' => $type)));

  // Return form for the given entity bundle.
  $entity = entity_create('foundation_block', array('type' => $type));
  $output = entity_ui_get_form('foundation_block', $entity);
  return $output;
}

/**
 * Form constructor for "foundation_block_bundle_1" bundle.
 *
 * This function is necessary for entity_ui_get_form() function.
 *
 * @see entity_ui_get_form()
 *
 * @param object $entity
 *   Enity to edit.
 *
 * @return array
 *   Entity edit form.
 */
function foundation_block_edit_foundation_block_bundle_1_form($form, &$form_state, $entity) {
  return foundation_block_form($form, $form_state, $entity);
}

/**
 * Form constructor for "foundation_block_bundle_2" bundle.
 *
 * This function is necessary for entity_ui_get_form() function.
 *
 * @see entity_ui_get_form()
 *
 * @param object $entity
 *   Enity to edit.
 *
 * @return array
 *   Entity edit form.
 */
function foundation_block_edit_foundation_block_bundle_2_form($form, &$form_state, $entity) {
  return foundation_block_form($form, $form_state, $entity);
}

/**
 * Form constructor for the entity edit form.
 *
 * In this form we shall manually define all form elements related to editable
 * properties of the entity.
 *
 * @param object $entity
 *   Enity to edit.
 *
 * @return array
 *   Entity edit form.
 */
function foundation_block_form($form, &$form_state, $entity) {
  // Store the entity in the form.
  $form_state['foundation_block'] = $entity;

  // Describe all properties of the entity which shall be shown on the form.
  $wrapper = entity_metadata_wrapper('foundation_block', $entity);
  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Title'),
    '#default_value' => $wrapper->title->value(),
    '#weight' => -20,
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $wrapper->description->value(),
    '#weight' => -10,
  );

  // Add fields of the entity to the form.
  field_attach_form('foundation_block', $entity, $form, $form_state);

  // Add some buttons.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if (empty($entity->is_new)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('foundation_block_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Entity edit form "Save" submit handler.
 */
function foundation_block_form_submit(&$form, &$form_state) {

  // Automatically get edited entity from the form and save it.
  // @see entity_form_submit_build_entity()
  $entity = $form_state['foundation_block'];
  // Add in created and changed times.
  if ($entity->is_new = isset($entity->is_new) ? $entity->is_new : 0){
    $entity->created = time();
  }
  $entity->changed = time();
  entity_form_submit_build_entity('foundation_block', $entity, $form, $form_state);
  $wrapper = entity_metadata_wrapper('foundation_block', $entity);
  $wrapper->save();

  // Redirect user to edited entity page.
  $entity_uri = entity_uri('foundation_block', $entity);
  $form_state['redirect'] = $entity_uri['path'];
}

/**
 * Entity edit form "Delete" submit handler.
 */
function foundation_block_form_submit_delete($form, &$form_state) {
  // Redirect user to "Delete" URI for this entity.
  $entity = $form_state['foundation_block'];
  $entity_uri = entity_uri('foundation_block', $entity);
  $form_state['redirect'] = $entity_uri['path'] . '/delete';
}

/**
 * Form constructor for the entity delete confirmation form.
 *
 * @param object $entity
 *   Entity to delete.
 *
 * @return array
 *   Confirmation form.
 */
function foundation_block_form_delete($form, &$form_state, $entity) {
  // Store the entity in the form.
  $form_state['foundation_block'] = $entity;

  // Show confirm dialog.
  $entity_uri = entity_uri('foundation_block', $entity);
  $message = t('Are you sure you want to delete foundation_block %title?', array('%title' => entity_label('foundation_block', $entity)));
  return confirm_form(
    $form,
    $message,
    $entity_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Entity "Confirm delete" form submit handler.
 */
function foundation_block_form_delete_submit($form, &$form_state) {
  // Delete the entity.
  $entity = $form_state['foundation_block'];
  $wrapper = entity_metadata_wrapper('foundation_block', $entity);
  $wrapper->delete();

  // Redirect user.
  drupal_set_message(t('Foundation Block %title deleted.', array('%title' => entity_label('foundation_block', $entity))));
  $form_state['redirect'] = '<front>';
}
