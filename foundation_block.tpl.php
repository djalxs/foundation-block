<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
dpm($content);
$data=array();
foreach($content['data']['#items'][0]['value'] as $item=>$value) {
  $data[$item] = $value;
}
$description = $content['description']['#items'][0]['value'];

if($data['show_button']) {
  $status_class = 'medium-7';
} else {
  $status_class = 'medium-9';
}
?>
<div class="foundation-block--progress">
<h2><?php echo $title; ?></h2>
  <div class="promo-bar">

    <div class="row" data-equalizer>
      <div class="column medium-3" data-equalizer-watch>
        <h2 class="promo-bar-title"><?php echo $title; ?></h2>
      </div>
      <div class="<?php echo $status_class; ?> column promo-bar-status" data-equalizer-watch>
        <p><?php echo $description; ?></p>
        <div class="progress large" role="progressbar" tabindex="0" aria-valuenow="<?php echo $data['current_value']; ?>" aria-valuemin="<?php echo $data['min_value']; ?>" aria-valuetext="<?php echo $data['current_value']; ?>" aria-valuemax="<?php echo $data['max_value']; ?>">
          <span class="progress-meter" style="width: <?php echo $data['slider_value']; ?>%">
            <p class="progress-meter-text"><?php echo $data['label_value']; ?></p>
          </span>
        </div>
      </div>
      <?php if($data['show_button']) { ?>
        <div class="column medium-2 promo-bar-donate" data-equalizer-watch>
            <a href="<?php echo $data['link_url']; ?>"><?php echo $data['link_text']; ?></a>
        </div>
      <?php } ?>
    </div>

  </div>
</div>

